cd /app/testdata/

#wait for the SQL Server to come up
sleep 60s

#run the setup script to create the DB and the schema in the DB
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Yukon900 -d master -i crebas.sql

#import the data from the csv file
/opt/mssql-tools/bin/bcp DeployLog.dbo.Deployments in "Deployments.csv" -c -t',' -S localhost -U sa -P Yukon900

