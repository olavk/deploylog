CREATE DATABASE DeployLog;
GO
USE DeployLog;
GO
CREATE TABLE Deployments (
    ID int identity(1,1),
    TimeStamp datetime, 
    AppId varchar(30), 
    AppName varchar(100),  
    AppVersion varchar(10),
    Description varchar(250)
);
GO

