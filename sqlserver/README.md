# Overview

This is a demo application for a deploy log-application running with Node on mssql-server.
Application and db-store will be separated


# Running the Demo
## Setting up the application and building the image for the first time
First, create a folder on your host and then clone this project into that folder:
```
To run the demo you just need to build the container:
```
make build
```

Then, you need to run the container:
```
make run
```
To show the web service response, open up a browser and point it to http://localhost:8080.

# Detailed Explanation
Here's a detailed look at each of the files in the project.  

## Dockerfile
The Dockerfile defines how the image will be built.  Each of the commands in the Dockerfile is described below.

The Dockerfile can define a base image as the first layer.  In this case, the Dockerfile uses the official Microsoft SQL Server Linux image that can be found on [Docker Hub](http://hub.docker.com/r/microsoft/mssql-server-linux).  The Dockerfile will pull the image with the 'latest' tag.  This image requires two environment variables to be passed to it at run time - `ACCEPT_EULA` and `SA_PASSWORD`.  The Microsoft SQL Server Linux inmage is in turn based on the official Ubuntu Linux image `Ubuntu:16.04`.

```
FROM microsoft/mssql-server-linux:latest
```

This RUN command will update all the installed packages in the image, install the curl utility if it is not already there and then install node.

``` 
RUN apt-get -y update  && \
        apt-get install -y curl && \
        curl -sL https://deb.nodesource.com/setup_6.x | bash - && \
        apt-get install -y nodejs
```

This installs the tedious driver for SQL Server which allows node applications to connect to SQL Server and run SQL commands.  This is an open source project to which Microsoft is now one of the main contributors.

[NPM package details](https://www.npmjs.com/package/tedious)

[Source Code](https://github.com/tediousjs/tedious)

```
RUN npm install tedious
```

This RUN command creates a new directory _inside_ the container at /usr/src/app and then sets the working directory to that directory.

``` 
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
```

Then this command copies the package.json file from the source code in this project to the /usr/src/app directory _inside_ the container.  The RUN command npm install will install all the dependencies defined in the package.json file.

```
COPY package.json /usr/src/app/
RUN npm install
```

Then all the source code from the project is copied into the container image in the /usr/src/app directory.
```
COPY . /usr/src/app
```

The EXPOSE command defines which port the application will be accessible at from outside the container.
```
EXPOSE 8080
```

Lastly, the CMD command defines what will be executed when the container starts.  In this case, it will execute the entrypoint.sh script contained in the source code for this project.  The souce code including the entrypoint.sh is contained in the /usr/src/app directory which has also been made the working directory by the commands above.
```
CMD /bin/bash ./entrypoint.sh
```

## entrypoint.sh
The entrypoint.sh script is executed when the conatiner first starts.  The script kicks off three things _simultaneously_:
* Start SQL Serevr using the sqlservr.sh script.  This script will look for the existence of the `ACCEPT_EULA` and `SA_PASSWORD` environment variables.  Since this will be the first execution of SQL Server the SA password will be set and then the sqlservr process will be started.  Note: Sqlservr runs as a process inside of a container, _not_ as a daemon.
* Executes the import-data.sh script contained in the source code of this project.  The import-data.sh script creates a database, populates the schema and imports some data.
* Runs npm start which will start the node application.
```
/opt/mssql/bin/sqlservr.sh & /usr/src/app/import-data.sh & npm start 
```

## setup.sh
The import-data.sh script is a convenient way to delay the execution of the SQL commands until SQL Server is started.  Typically SQL Server takes about 5-10 seconds to start up and be ready for connections and commands.  Bringing the SQL commands into a separate .sh script from entrypoint.sh creates modularity between the commands that should be run at container start up time and the SQL commands that need to be run.  It also allow for the container start up commands to be run immediately and the SQL commands to be delayed.

This command causes a wait to allow SQL Server to start up.  Nintey seconds is a bit excessive, but will ensure that even if there are extraordinary delays that the scripts will not execute until SQL Server is up.  For demo purposes you may want to reduce this number.
```
sleep 90s
```

The next command uses the SQL Server command line utility sqlcmd to execte some SQL commands contained in the setup.sql file.  The commands can also be passed directly to sqlcmd via the -q parameter.  For better readibility if you have lots of SQL commands, it's best to create a separate .sql file and put all the SQL commands in it. 

**IMPORTANT:** Make sure to change your password here if you use something other than 'Yukon900'.

```
sqlcmd -S localhost -U sa -P Yukon900 -d master -i crebas.sql
```

The crebas.sql script will create a new database called `DeployLog` and a table called `Deployments` in the default `dbo` schema.  This bcp command will import the data contained in the source code file Deployments.csv.
**IMPORTANT:** If you change the names of the database or the table in the setup.sql script, make sure you change them here too.
**IMPORTANT:** Make sure to change your password here if you use something other than 'Yukon900'.

```
bcp DeployLog.dbo.Deployments in "/usr/src/app/testdata/Deployments.csv" -c -t',' -S localhost -U sa -P Yukon900
```

## crebas.sql
The crebas.sql defines some simple commands to create a database and some simple schema.  You could use a .sql file like this for other purposes like creating logins, assigning permissions, creating stored procedures, and much more.  When creating a database in production situations, you will probably want to be more specific about where the database files are created so that the database files are stored in persistent storage.  This SQL script creates a table with two columns - ID (integer) and ProductName (nvarchar(max)).
```

## Deployments.csv
This CSV data file contains some sample data to populate the Deployments table. The bcp command in the setup.sh script uses this file to import the data into the Deployments table created by the creabas.sql script file.
```

## server.js
The server.js file defines the node application that exposes the web service and retrieves the data from SQL Server and returns it to the requestor as a JSON response.

The require statements at the top of the file bring in some libraries like tedious and express and define some global variables which can be used by the rest of the application.
```
var express = require("express");
var app = express();
var connection = require('tedious').Connection;
var request = require('tedious').Request;
```
The app.get defines the _route_ for this application.  Any GET request that comes to the root of this application will be handled by this function.  This effectively creates a simple REST-style interface for returing data in JSON from a GET request.
```
app.get('/', function (req, res) {
```

The next set of commands define the connection parameters and creates a connection object.

**IMPORTANT:** Make sure to change your password here if you use something other than 'Yukon900'.

**IMPORTANT:** If you change the names of the database in the setup.sql script, make sure you change it here to.
```
var config = {
     userName: 'sa',
     password: 'Yukon900', // update me
     server: 'localhost',
     options: {
         database: 'DemoData'
     }
}
var conn = new connection(config);
```

This next comamnd defines the event handler function for the connection.on event.
```
conn.on('connect', function(err) {
```

Assuming the connection is made correctly, the next command sets up the query that will be executed.  This uses SQL Server's built in JSON functions to retrieve the data in JSON format for us so we don't have to write code to convert the data from a traditional rowset into JSON. Nice!

[More information on JSON in SQL server](https://msdn.microsoft.com/en-us/library/dn921897.aspx)

```
sqlreq = new request("SELECT * FROM Products FOR JSON AUTO", function(err, rowCount) {
```

The next set of commands set up the event handler function for the sql request row command which will be triggered for each row in a response.  In this case there will only be a single row and a single column because we are using `FOR JSON AUTO` to get the data returned in a single string of JSON data.   Assuming the request comes back with a row and a column value we simply return the JSON string (the column.value) directly to the browser in the response (res).
```
sqlreq.on('row', function(columns) { 
   columns.forEach(function(column) {  
      if (column.value === null) {  
         console.log('NULL');
      } else {  
         res.send(column.value);
      }  
   });
});
```

This is the command that actually sends in the SQL request:
```
conn.execSql(sqlreq); 
```

This command starts the app listening on port 8080.
**IMPORTANT:** If you change the port number in the Dockerfile EXPOSE command make sure you change it here too.

```
var server = app.listen(8080, function () {
    console.log("Listening on port %s...", server.address().port);
});
```

## Alternative approach using SQL scripts for seeding schema and data

So far, the steps above have described how to create a docker image of sqlserver that starts seeding on first run.
However, if we have a huge database to be seeded, the setup.sql could contain all the 
CREATE/INSERT SQL statements that are needed for seeding the database. We do not have to import it from csv file.

You could either add the CREATE/INSERT statements to setup.sql and have those run each time a container is created or you can create a new image that has the schema and data captured inside of it.  In that case, after starting a new container and executing the .sql script, we can commit the newly running container with seeded db as a new image using "docker commit" command.
```
docker commit <container_id> <docker image tag>
```

We could now use this new image in a new docker based project including in a Docker Compose app using a docker-compose.yml file.

Also node.js dependency can be removed in this case. Node.js is only used here as an example web service to show the data can be retrieved from the SQL Server.

